## [4.2.1](https://gitlab.com/to-be-continuous/node/compare/4.2.0...4.2.1) (2025-01-31)


### Bug Fixes

* **sbom:** only generate SBOMs on prod branches, integ branches and release tags ([b61bdcd](https://gitlab.com/to-be-continuous/node/commit/b61bdcdc294567e78771e83fc2fceb122a3c16fe))

# [4.2.0](https://gitlab.com/to-be-continuous/node/compare/4.1.1...4.2.0) (2025-01-27)


### Features

* disable tracking service by default ([8823fa4](https://gitlab.com/to-be-continuous/node/commit/8823fa457209e3f38e09d70f0a3b755611394ed4))

## [4.1.1](https://gitlab.com/to-be-continuous/node/compare/4.1.0...4.1.1) (2024-12-05)


### Bug Fixes

* semgrep subdir ([8ac460c](https://gitlab.com/to-be-continuous/node/commit/8ac460c05668590b7713f05fc571fc7b3fe2f4b4))

# [4.1.0](https://gitlab.com/to-be-continuous/node/compare/4.0.3...4.1.0) (2024-11-23)


### Features

* **lint:** enforce GitLab integration ([44cc3c3](https://gitlab.com/to-be-continuous/node/commit/44cc3c30b1211f35c428edfc5e4165aaae086fbc))

## [4.0.3](https://gitlab.com/to-be-continuous/node/compare/4.0.2...4.0.3) (2024-11-02)


### Bug Fixes

* limit security reports access to developer role or higher ([3d4335f](https://gitlab.com/to-be-continuous/node/commit/3d4335fd9d46070720de57cda656c2570dd9efa2))

## [4.0.2](https://gitlab.com/to-be-continuous/node/compare/4.0.1...4.0.2) (2024-10-04)


### Bug Fixes

* **release:** support full semantic-versioning specifcation (with prerelease and build metadata) ([b031dae](https://gitlab.com/to-be-continuous/node/commit/b031dae19ccf2cd4f31066ff052efd2d8b1c7aef))

## [4.0.1](https://gitlab.com/to-be-continuous/node/compare/4.0.0...4.0.1) (2024-09-08)


### Bug Fixes

* set node-publish default registry ([fd0f04d](https://gitlab.com/to-be-continuous/node/commit/fd0f04d730e7b844dd6f57a77b21d913b16dd1c9))

# [4.0.0](https://gitlab.com/to-be-continuous/node/compare/3.15.2...4.0.0) (2024-07-15)


### Bug Fixes

* when enabled, build should be always run ([5175c16](https://gitlab.com/to-be-continuous/node/commit/5175c1655ce97d28b9719e9cc193eeb754a39476))


### BREAKING CHANGES

* restoring the expected behavior might break some projects pipeline

## [3.15.2](https://gitlab.com/to-be-continuous/node/compare/3.15.1...3.15.2) (2024-06-17)


### Bug Fixes

* log output for semgrep download ([627482e](https://gitlab.com/to-be-continuous/node/commit/627482ed0d48116da4d2f0b3a3117117fbb739c5))

## [3.15.1](https://gitlab.com/to-be-continuous/node/compare/3.15.0...3.15.1) (2024-06-13)


### Bug Fixes

* **build:** fix NODE_BUILD_DISABLED param ([e97d77d](https://gitlab.com/to-be-continuous/node/commit/e97d77d5f4288f0e104bb3d251e026d19954d4fa))

# [3.15.0](https://gitlab.com/to-be-continuous/node/compare/3.14.0...3.15.0) (2024-06-13)


### Features

* configurable Semgrep Registry base URL ([d9c2041](https://gitlab.com/to-be-continuous/node/commit/d9c20417173392b7cdbb60bf0d257c136c238daf))

# [3.14.0](https://gitlab.com/to-be-continuous/node/compare/3.13.0...3.14.0) (2024-06-05)


### Features

* fail Semgrep on internal errors (wrong ruleset or else) ([a7df396](https://gitlab.com/to-be-continuous/node/commit/a7df39621772079ba2403ceb09c964da970bb2e7))

# [3.13.0](https://gitlab.com/to-be-continuous/node/compare/3.12.3...3.13.0) (2024-05-17)


### Features

* add Semgrep job ([6538ba7](https://gitlab.com/to-be-continuous/node/commit/6538ba71697d4a6e08561206795c3fb6c275de69))

## [3.12.3](https://gitlab.com/to-be-continuous/node/compare/3.12.2...3.12.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([bbcf708](https://gitlab.com/to-be-continuous/node/commit/bbcf70880021196b3003aadc727fafb7936c24c4))

## [3.12.2](https://gitlab.com/to-be-continuous/node/compare/3.12.1...3.12.2) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([4413b68](https://gitlab.com/to-be-continuous/node/commit/4413b682db377e48d6a8bcfcd2f585d406c39bea))

## [3.12.1](https://gitlab.com/to-be-continuous/node/compare/3.12.0...3.12.1) (2024-03-31)


### Bug Fixes

* **node-lint:** continue on lint report generation for SonarQube errors ([b50cbe7](https://gitlab.com/to-be-continuous/node/commit/b50cbe7771a3a94fabff6b8a3aac8ccfcd531c62))

# [3.12.0](https://gitlab.com/to-be-continuous/node/compare/3.11.1...3.12.0) (2024-2-26)


### Features

* add Vault variant ([ad13518](https://gitlab.com/to-be-continuous/node/commit/ad13518009ee6a4a7a08c71325964ef80207eb49))

## [3.11.1](https://gitlab.com/to-be-continuous/node/compare/3.11.0...3.11.1) (2024-1-28)


### Bug Fixes

* check that pnpm is installed before using it ([891e4f7](https://gitlab.com/to-be-continuous/node/commit/891e4f75b42115561d7df47c33fc06d68be0e18a))

# [3.11.0](https://gitlab.com/to-be-continuous/node/compare/3.10.1...3.11.0) (2024-1-27)


### Features

* migrate to CI/CD component ([f7fa527](https://gitlab.com/to-be-continuous/node/commit/f7fa527af0eb6c85362ba49a84e1902a96aa0b25))

## [3.10.1](https://gitlab.com/to-be-continuous/node/compare/3.10.0...3.10.1) (2024-1-7)


### Bug Fixes

* install dependencies for pnpm ([473abd2](https://gitlab.com/to-be-continuous/node/commit/473abd2932adf111f83522284a7235a3e5528b05)), closes [#29](https://gitlab.com/to-be-continuous/node/issues/29)
* typo in pnpm lock file ([c6d81da](https://gitlab.com/to-be-continuous/node/commit/c6d81da52698801695d75a6679c1ac21e0e675f9)), closes [#28](https://gitlab.com/to-be-continuous/node/issues/28)

# [3.10.0](https://gitlab.com/to-be-continuous/node/compare/3.9.0...3.10.0) (2023-12-18)


### Features

* add pnpm support ([6812cf7](https://gitlab.com/to-be-continuous/node/commit/6812cf7df9fde7cdd9625d62369f5ef1d04b1756))

# [3.9.0](https://gitlab.com/to-be-continuous/node/compare/3.8.0...3.9.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([edf85be](https://gitlab.com/to-be-continuous/node/commit/edf85be8c983743cc192c5dfb8eb78dd9154e573))

# [3.8.0](https://gitlab.com/to-be-continuous/node/compare/3.7.1...3.8.0) (2023-11-25)


### Features

* support multiple auth methods ([870de1d](https://gitlab.com/to-be-continuous/node/commit/870de1da8f1631cadf91d6d43a3176fa97dc1f2c))

## [3.7.1](https://gitlab.com/to-be-continuous/node/compare/3.7.0...3.7.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([0e75618](https://gitlab.com/to-be-continuous/node/commit/0e75618d91112931b1b4577f77dfd339e29d6c46))

# [3.7.0](https://gitlab.com/to-be-continuous/node/compare/3.6.0...3.7.0) (2023-08-28)


### Features

* **publish:** add publish job ([a271c64](https://gitlab.com/to-be-continuous/node/commit/a271c6454bd6589778861c2d6e55e7964d96f6d9))

# [3.6.0](https://gitlab.com/to-be-continuous/node/compare/3.5.3...3.6.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([d79ef3b](https://gitlab.com/to-be-continuous/node/commit/d79ef3bb24f6def7e27880bd8566d5e409a21a64))

## [3.5.3](https://gitlab.com/to-be-continuous/node/compare/3.5.2...3.5.3) (2023-03-28)


### Bug Fixes

* **sbom:** add CycloneDX report ([d70030a](https://gitlab.com/to-be-continuous/node/commit/d70030a34bf9110b3c8ee5b7e570a3aed2dfdb5f))

## [3.5.2](https://gitlab.com/to-be-continuous/node/compare/3.5.1...3.5.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([4229c36](https://gitlab.com/to-be-continuous/node/commit/4229c36c3544af579c031ccaf61bcf5358a09ef6))

## [3.5.1](https://gitlab.com/to-be-continuous/node/compare/3.5.0...3.5.1) (2022-12-11)


### Bug Fixes

* **audit:** npm audit has no upstream deps ([e53d347](https://gitlab.com/to-be-continuous/node/commit/e53d3471859bcc42d2ab196ab84d4106d0ec0527))

# [3.5.0](https://gitlab.com/to-be-continuous/node/compare/3.4.2...3.5.0) (2022-12-01)


### Features

* add a job generating software bill of materials ([fb0adc2](https://gitlab.com/to-be-continuous/node/commit/fb0adc2d7f4cc96dcd17f3c689d980af5c9f7350))

## [3.4.2](https://gitlab.com/to-be-continuous/node/compare/3.4.1...3.4.2) (2022-12-01)


### Bug Fixes

* **audit:** Generate NPM audit and outdated reports ([944f8ce](https://gitlab.com/to-be-continuous/node/commit/944f8cec1bdee7e8fe0905089d8a1debf94e06c7))

## [3.4.1](https://gitlab.com/to-be-continuous/node/compare/3.4.0...3.4.1) (2022-11-26)


### Bug Fixes

* support custom CA certificates with npm ([8370e7a](https://gitlab.com/to-be-continuous/node/commit/8370e7ac69985b6b382289fb74a38d8e84d4dded))

# [3.4.0](https://gitlab.com/to-be-continuous/node/compare/3.3.0...3.4.0) (2022-11-23)


### Bug Fixes

* **cache:** absolute cache dir variables ([088dce0](https://gitlab.com/to-be-continuous/node/commit/088dce0f6900418d8b4b77487f8497a7e8dec138))


### Features

* support extra opts to install project deps ([363b961](https://gitlab.com/to-be-continuous/node/commit/363b961aebdbc76e8010165be438189049947592))

# [3.3.0](https://gitlab.com/to-be-continuous/node/compare/3.2.1...3.3.0) (2022-10-18)


### Features

* revert node-outdated behavior to warning only ([800b4e8](https://gitlab.com/to-be-continuous/node/commit/800b4e82f6d9a9c28f243796cd2364726337abdd))

## [3.2.1](https://gitlab.com/to-be-continuous/node/compare/3.2.0...3.2.1) (2022-10-14)


### Bug Fixes

* **lint:** support official SONAR_HOST_URL to trigger JSON report ([54e657d](https://gitlab.com/to-be-continuous/node/commit/54e657df3dbc98662d44ce26e5228beb3bc6c32f))

# [3.2.0](https://gitlab.com/to-be-continuous/node/compare/3.1.0...3.2.0) (2022-10-13)


### Features

* **coverage:** add Cobertura support ([478dd58](https://gitlab.com/to-be-continuous/node/commit/478dd58094d6040d27eeb3cdf5551041f8dfffeb))

# [3.1.0](https://gitlab.com/to-be-continuous/node/compare/3.0.1...3.1.0) (2022-10-04)


### Features

* normalize reports ([fc24665](https://gitlab.com/to-be-continuous/node/commit/fc24665e232311792b00e9de2032a50db5321c9f))

## [3.0.1](https://gitlab.com/to-be-continuous/node/compare/3.0.0...3.0.1) (2022-10-01)


### Bug Fixes

* **yarn:** remove deprecated --cache-folder option ([ef09432](https://gitlab.com/to-be-continuous/node/commit/ef09432404a8ecafdbed8a1518a2c2a818872261))

# [3.0.0](https://gitlab.com/to-be-continuous/node/compare/2.4.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([ed3af02](https://gitlab.com/to-be-continuous/node/commit/ed3af028e090fa6deb8dd20ca64039a8f14bacd3))
* switch to MR pipeline as default ([4e66d5d](https://gitlab.com/to-be-continuous/node/commit/4e66d5d5d0240792bd86d1ea705ec60f09a2c553))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.4.0](https://gitlab.com/to-be-continuous/node/compare/2.3.0...2.4.0) (2022-08-02)


### Bug Fixes

* **cache:** Using variable $NODE_MANAGER won't work if auto-detection is enabled (cache path is evaluated before executing our scripts) ([b54c72a](https://gitlab.com/to-be-continuous/node/commit/b54c72a6e3e7c45d9f5e807b4c44f32d32834980))
* **config_registry:** rename var ([e43446a](https://gitlab.com/to-be-continuous/node/commit/e43446a39694ce41ca45f9a6ac529403d788aba0))
* hardcode all variabilized report variables ([ab797c2](https://gitlab.com/to-be-continuous/node/commit/ab797c255b18cd2ec0f0281a1ca676ed754daa3d))
* **jest:** junit_test_report as file name instead of unit_test_report ([6d63868](https://gitlab.com/to-be-continuous/node/commit/6d6386889fe06fe66a80ca64281b46a6b33cedac))
* **jest:** revert junit_test_report as file name ([1050908](https://gitlab.com/to-be-continuous/node/commit/1050908858f6112289c60430ceda954c845b07f6))
* **node-audit:** generate json report only on job failure ([7270fd3](https://gitlab.com/to-be-continuous/node/commit/7270fd3ee26a6f5233890ede00ca74eb3ebdd3af))
* **node-build:** remove --bail option from NODE_TEST_ARGS default value ([584e880](https://gitlab.com/to-be-continuous/node/commit/584e880123c86d9263d7c5edf1afa2447c680ae2))
* **node-outdated:** generate json report only on job failure ([481783c](https://gitlab.com/to-be-continuous/node/commit/481783c2bb1a4d37ac4e1be59010189184e41fe3))
* **node-outdated:** NODE_OUTDATED_DISABLED should be a boolean ([822c7a6](https://gitlab.com/to-be-continuous/node/commit/822c7a64d859fc65785a1a5d06707c0dc088ab36))
* **node-outdated:** remove silent parameter (bad practice) ([46b049b](https://gitlab.com/to-be-continuous/node/commit/46b049be8d75f3171c07936b521ee9d66c0ecc8b))


### Features

* multiple node manager support (npm and yarn) ([d1ab6f8](https://gitlab.com/to-be-continuous/node/commit/d1ab6f83394c82130b93326a411362b39e069a38))
* remove html reports ([937671d](https://gitlab.com/to-be-continuous/node/commit/937671d2da66a35251634b0651da05ce2ccf4ddc))
* remove node-js-scan job ([48f73af](https://gitlab.com/to-be-continuous/node/commit/48f73afbcd6398431393f19e0e8fca9b84a8a91a))
* silent mode for node-outdated job ([8125cfa](https://gitlab.com/to-be-continuous/node/commit/8125cfa863164c41d94e84d51faa7601ac58ab69))

# [2.3.0](https://gitlab.com/to-be-continuous/node/compare/2.2.0...2.3.0) (2022-05-01)


### Features

* configurable tracking image ([545d836](https://gitlab.com/to-be-continuous/node/commit/545d8361de9cecfa6dabd0f77ea023980b50d207))

# [2.2.0](https://gitlab.com/to-be-continuous/node/compare/2.1.0...2.2.0) (2022-01-04)


### Features

* **npmoutdated:** add npm outdated job ([3c6c261](https://gitlab.com/to-be-continuous/node/commit/3c6c2610d994ed822a0331629570f025279a7d2a))

# [2.1.0](https://gitlab.com/to-be-continuous/node/compare/2.0.2...2.1.0) (2021-11-23)


### Features

* **nodejsscan:** add SARIF output format ([b95ca31](https://gitlab.com/to-be-continuous/node/commit/b95ca3108a05a892313d8b6bfd4068404b73742c)), closes [#9](https://gitlab.com/to-be-continuous/node/issues/9)

## [2.0.2](https://gitlab.com/to-be-continuous/node/compare/2.0.1...2.0.2) (2021-10-13)


### Bug Fixes

* eslint report JSON format ([64432f2](https://gitlab.com/to-be-continuous/node/commit/64432f24813341e680b5f7af74f08076d30ddede))

## [2.0.1](https://gitlab.com/to-be-continuous/node/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([f3285e7](https://gitlab.com/to-be-continuous/node/commit/f3285e7f452e8d1ce92f38a3bc0bbdc891983d4c))

## [2.0.0](https://gitlab.com/to-be-continuous/node/compare/1.2.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([6d4ba9d](https://gitlab.com/to-be-continuous/node/commit/6d4ba9ddc6a4cd1e670ab6dcb21f1e832894255e))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.0](https://gitlab.com/to-be-continuous/node/compare/1.1.1...1.2.0) (2021-06-11)

### Features

* move group ([4d3219b](https://gitlab.com/to-be-continuous/node/commit/4d3219b87eb2e8eb2b09cf560bce09abb28aa027))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/node/compare/1.1.0...1.1.1) (2021-05-19)

### Bug Fixes

* **lint:** remove useless --force param ([347faa8](https://gitlab.com/Orange-OpenSource/tbc/node/commit/347faa83d11a1d613d1bd479ec103cea62db89ec))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/node/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([bc25fa0](https://gitlab.com/Orange-OpenSource/tbc/node/commit/bc25fa02e8052973b932273605ece936718f3fe3))

## 1.0.0 (2021-05-06)

### Features

* initial release ([07fdee7](https://gitlab.com/Orange-OpenSource/tbc/node/commit/07fdee71d89f3f3413a64cf6151d0bb4a0daa971))
